const express = require('express');

const router = express.Router();

const Employees = require('../models/employees');

router.get('/', async (req, res) => {

    try {
        const Employeess = await Employees.find();
        res.json(Employeess)

    } catch (error) {
        res.send('Error' + error);
    }
});

router.get('/api/:id', async (req, res) => {

    try {
        const Employeess = await Employees.findById(req.params.id);
        res.json(Employeess)

    } catch (error) {
        res.send('Error' + error);
    }
});

router.post('/', async (req, res) => {

    const employee = new Employees({
        name: req.body.name,
        designation: req.body.designation,
        DOB: req.body.DOB
    });
    try {

        const employeeResponse = await employee.save();
        res.send(employeeResponse);

    } catch (error) {
        res.send(error);
    }
});

router.patch('/:id', async (req, res) => {
    try {
        const editEmployee = await Employees.findById(req.params.id);
        editEmployee.name = req.body.name;

        const saveEmployee = await editEmployee.save();
        res.json(saveEmployee);

    } catch (error) {
        res.send(error);
    }

});


// Handling update through post
router.post('/:id', async (req, res) => {

    try {
        const editEmployee = await Employees.findById(req.params.id);
        editEmployee.name = req.body.name;
        editEmployee.designation = req.body.designation;
        editEmployee.DOB = req.body.DOB;

        const saveEmployee = await editEmployee.save();
        res.json(saveEmployee);

    } catch (error) {
        res.send(error);
    }

});


router.delete('/:id', async (req, res) => {

    try {
        const delEmployee = await Employees.findById(req.params.id);
        const saveEmployee = await delEmployee.delete();
        res.send("Deleted Successfully");

    } catch (error) {
        res.send(error);
    }

});
module.exports = router;