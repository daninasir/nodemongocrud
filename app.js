const express = require('express');
const mongoose = require('mongoose');

// const url = 'mongodb://localhost/BahriaTown';

const url = 'mongodb+srv://admin:admin@cluster0.89nh0.mongodb.net/Maju?retryWrites=true&w=majority';
const PORT = process.env.PORT || 8000;

const app = express();
mongoose.connect(url, { useNewUrlParser: true });

const con = mongoose.connection;

con.on('open', () => {
    console.log("connected..");
})

app.use(express.json());

const employeesRouter = require('./routers/employees');
app.use('/employees', employeesRouter);

app.listen(PORT, () => {
    console.log(`Server Started, Listening to port ${PORT}`);
});